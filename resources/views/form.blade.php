@extends('home')

@section('form')
    <div id="app">
        <zip-form-component countries='{{ $countries }}'></zip-form-component>
    </div>
@endsection
