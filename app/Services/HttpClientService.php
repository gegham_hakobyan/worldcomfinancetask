<?php

namespace App\Services;
use GuzzleHttp\Exception\ClientException;

class HttpClientService
{

    public static function getPlaces($data)
    {
        $client =  new \GuzzleHttp\Client;
        $uri = 'https://api.zippopotam.us/' .  $data['abbr'] . '/' . $data['zipCode'];

        try {
            $response = $client->get($uri);
            $result = ['body' => $response->getBody()->getContents(), 'statusCode' => $response->getStatusCode()];
        } catch (ClientException $exception) {
            $result = ['message' => $exception->getMessage(), 'statusCode' => $exception->getCode()];
        }

        return $result;

    }
}
