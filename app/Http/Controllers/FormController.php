<?php

namespace App\Http\Controllers;

use App\Models\Country;
use Illuminate\Http\Request;
use App\Models\ZipCode;

class FormController extends Controller
{
    public function index()
    {
        $countries = Country::all()->values()->toJson();
        return view('form', compact('countries'));
    }

    public function getPlaces(Request $request)
    {
        $this->validate(request(), [
           'abbr' => 'required',
           'zipCode' => 'required'
        ]);

        $zipCode = new ZipCode();
        $result =$zipCode->getPlaces($request);

        if(isset($result['statusCode']) && $result['statusCode'] == '404') {
            return response()->json([
                'status' => 'error',
                'msg' => $result['message'],
            ], 404);
        }


        return $result->values();
    }
}
