<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Services\HttpClientService;
use App\Models\Place;

class ZipCode extends Model
{
    protected $fillable = ['code', 'country_id'];

    public function country ()
    {
        return $this->belongsTo('App\Models\Country');
    }

    public function getPlaces($request)
    {
        $zipCode = $this->where('code', $request['zipCode'])->get();
        $country = new Country();
        $selectedCountry  = $country->getCountryByAbbr($request['abbr']);

        if(!$zipCode->isEmpty() &&
            $zipCode[0]->country->id === $selectedCountry->id)
        {
            return $zipCode[0]->places;
        }

        return $this->getPlacesFromApi($request);


    }

    private function getPlacesFromApi($request)
    {
        $result = HttpClientService::getPlaces($request);

        if (isset($result['body'])) {
            $this->saveZipCode($request);
            $place = new Place();
            return $place->savePlaces(json_decode($result['body']), true);

        } else {
            return $result;
        }
    }
    private function saveZipCode($data)
    {
        $country = new Country();
        $country = $country->getCountryByAbbr($data['abbr']);

        $zipCode = new ZipCode([
            'code' => $data['zipCode'],
            'country_id' => $country->id
        ]);

        $zipCode->save();

    }

    public function places ()
    {
        return $this->hasMany('App\Models\Place');
    }
}
