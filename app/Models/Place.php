<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Place extends Model
{
    protected $fillable = ['name','state', 'longitude', 'latitude', 'zip_code_id'];

    public function zipCode()
    {
        return $this->hasOne('App\Model\ZipCode');
    }

    public static function savePlaces($data)
    {
        $zipCode = ZipCode::where('code', $data->{'post code'})->first();

        foreach ($data->places as $item) {
            $place = new Place([
                'name'=> $item->{'place name'},
                'state' => $item->state,
                'latitude' => $item->latitude,
                'longitude' => $item->longitude,
                'zip_code_id' => $zipCode->id
            ]);

            $place->save();
        }

        return $zipCode->places;
    }
}
