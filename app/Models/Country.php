<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $fillable = ['name', 'abbr'];

    public function zipCode ()
    {
        return $this->hasMany('App\Models\ZipCode');
    }

    public function getCountryByAbbr($abbr)
    {
        return $this->where('abbr', $abbr)->first();
    }
}
